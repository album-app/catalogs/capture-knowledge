This [album](https://album.solutions) catalog is deployed to https://solutions.capture-knowledge.org.

In order to add this catalog to album, run this command:

```
album add-catalog https://gitlab.com/album-app/catalogs/capture-knowledge
```
