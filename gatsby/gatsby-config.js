module.exports = {
  pathPrefix: `/`,
  siteMetadata: {
    title: 'capture-knowledge',
    subtitle: 'a repository of album solutions to capture knowledge',
    catalog_url: 'https://gitlab.com/album-app/catalogs/capture-knowledge',
    menuLinks:[
      {
         name:'Catalog',
         link:'/catalog'
      },
      {
         name:'About',
         link:'/about'
      },
    ],
  },
  plugins: [{ resolve: `gatsby-theme-album`, options: {} }],
}
