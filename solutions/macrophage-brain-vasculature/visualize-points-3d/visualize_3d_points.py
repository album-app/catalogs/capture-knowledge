import math
import pandas as pd

from scyjava import config, jimport

# headless
config.add_option('-Xmx150g')
# config.add_option('-Djava.awt.headless=true')
config.add_repositories({'scijava.public': 'https://maven.scijava.org/content/groups/public'})
config.add_endpoints('net.imagej:imagej:2.3.0')
config.add_endpoints('net.imagej:imagej-legacy:0.38.0')
HashMap = jimport("java.util.HashMap")

IJ = jimport('ij.IJ')
RoiManager = jimport('ij.plugin.frame.RoiManager')
Roi = jimport('ij.gui.Roi')
Duplicator = jimport('ij.plugin.Duplicator')
Slicer = jimport('ij.plugin.Slicer')
ZProjector = jimport('ij.plugin.ZProjector')
ImageProcessor = jimport('ij.process.ImageProcessor')
PointRoi = jimport('ij.gui.PointRoi')
ResultsTable = jimport('ij.measure.ResultsTable')

# Use these for visual debugging
ImageJ = jimport('ij.ImageJ')
imagej = ImageJ()

roimanager = RoiManager()


def visualize_points(image_filename, csv_filename, target_channel=2, sep=';'):
    # open csv and print info
    df = pd.read_csv(csv_filename, sep=sep)
    points = df[['time_point', 'number', 'X', 'Y', 'Z']].to_numpy()
    N = points.shape[0]
    print('Number of points: %d' % N)

    # open image
    imp = IJ.openImage(image_filename)
    imp.show()

    for k in range(N):
        # extract point
        point = points[k, :]
        frame = int(point[0])
        X = float(point[2])
        Y = float(point[3])
        Z = float(point[4])

        # stop when no Z coordinate found
        if math.isnan(Z):
            print("No Z coordinate found for point [%s, %s]" % (X, Y))
            continue

        # define a point ROI
        roi = PointRoi(X, Y)

        # info
        print("Adding point %s" % [
            k,
            int(X),
            int(Y), frame, target_channel,
            int(Z)
        ])

        # set the position
        roi.setPosition(target_channel, int(Z), frame)
        roimanager.addRoi(roi)

    print("Press Enter to continue. Close all ImageJ windows first...")
    input()


if __name__ == '__main__':
    print("Running visualize_points")
