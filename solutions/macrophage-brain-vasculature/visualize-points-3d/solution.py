from album.runner.api import get_args, setup

global args


def run():
    from visualize_3d_points import visualize_points

    args = get_args()
    visualize_points(args.image_filename, args.csv_filename, args.target_channel, args.separator)


setup(
    group="macrophage-brain-vasculature",
    name="visualize-points-3d",
    version="0.1.0-SNAPSHOT",
    title="macrophage-brain-vasculature visualize 3D points",
    description="A solution for visualizing 3D coordinates with respect to a target image",
    authors=["Kyle Harrington, Jan Philipp Albrecht"],
    tags=["vasculature", "segmentation", "projection"],
    license="Apache v2",
    documentation=["README.md"],
    covers=[{
        "description": "Single annotation of a Macrophage signal intensity",
        "source": "visualize_points_3d.png",
    }],
    album_api_version="0.4.2",
    args=[{
        "name": "csv_filename",
        "description": "csv filename"
    }, {
        "name": "image_filename",
        "description": "image filename"
    }, {
        "name": "target_channel",
        "description": "Target channel",
        "type": "integer",
        "default": 2
    }, {
        "name": "separator",
        "description": "Separator to use for reading in the csv file.",
        "type": "string",
        "default": ";"
    }],
    run=run,
    dependencies={
        'parent': {
            'group': 'macrophage-brain-vasculature',
            'name': 'mbv-parent',
            'version': '0.1.0-SNAPSHOT'
        }
    }
)
