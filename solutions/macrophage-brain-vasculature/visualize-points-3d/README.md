# Visualize the 3d coordinates of the macrophages.
This solution can be taken to visualize the 3D coordinates of the macrophages. 
The solution requires the csv file containing the 3D coordinates of the macrophages as input and
depicts each in 3D in the input image.

## How to run
Install album (https://album.solutions)[https://album.solutions], download the solution file and install and then run it.

## Parameters
The solution has the following parameters:
- `image_filename`: The input filename of the 4D image. The image should be a 4D image with the following dimensions: 
  - 0: Channels
  - 1: x dimension
  - 2: y dimension
  - 3: z dimension
- `csv_filename`: The csv file containing the 3D coordinates of the macrophages
- `target_channel`: The channel of the image where the macrophages are captured
- `debug`: If true, the intermediate results are shown

## How it works
ImageJ is used to visualize the 3D coordinates of the macrophages. The 4D image is first converted to a 3D image.
The 3D coordinates of the macrophages that are provided by the csv input file are then shown.