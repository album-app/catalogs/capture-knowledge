import argparse
import os
import signal

import numpy as np
import pandas as pd
from scyjava import config, jimport


def do_jimport(debug=False):
    # headless
    config.add_option('-Xmx100g')
    if not debug:
        config.add_option('-Djava.awt.headless=true')

    # add scyjava repositories and endpoints
    config.add_repositories({'scijava.public': 'https://maven.scijava.org/content/groups/public'})
    config.endpoints.append('net.imagej:imagej:2.3.0')
    config.endpoints.append('net.imagej:imagej-legacy:0.38.0')
    # config.add_endpoints('net.imagej:imagej:2.3.0')
    # config.add_endpoints('net.imagej:imagej-legacy:0.38.0')
    HashMap = jimport("java.util.HashMap")

    # imagej imports
    IJ = jimport('ij.IJ')
    RoiManager = jimport('ij.plugin.frame.RoiManager')
    Roi = jimport('ij.gui.Roi')
    Duplicator = jimport('ij.plugin.Duplicator')
    Slicer = jimport('ij.plugin.Slicer')
    ZProjector = jimport('ij.plugin.ZProjector')
    MaximumFinder = jimport('ij.plugin.filter.MaximumFinder')
    ImageProcessor = jimport('ij.process.ImageProcessor')
    PointRoi = jimport('ij.gui.PointRoi')
    ResultsTable = jimport('ij.measure.ResultsTable')
    GaussianBlur3D = jimport('ij.plugin.GaussianBlur3D')

    # Use these for visual debugging
    if debug:
        ImageJ = jimport('ij.ImageJ')
        imagej = ImageJ()

    jimp_dict = {
        "IJ": IJ,
        "RoiManager": RoiManager,
        "Roi": Roi,
        "Duplicator": Duplicator,
        "Slicer": Slicer,
        "ZProjector": ZProjector,
        "MaximumFinder": MaximumFinder,
        "ImageProcessor": ImageProcessor,
        "PointRoi": PointRoi,
        "ResultsTable": ResultsTable,
        "GaussianBlur3D": GaussianBlur3D,
    }

    return jimp_dict


def find_points(image_filename, csv_filename, output_filename, sigma, debug):
    jimp_dict = do_jimport(debug)
    # ######### options #########
    target_channel = 2
    half_region_size = [25.0, 25.0]  # affects ROI size

    # z projection find maximum options
    tolerance = 4  # can affect behavior
    maxima_mode = 4  # 3 is "POINT_SELECTION" in imagej, 0 is "SINGLE_POINTS", 4 is LIST
    maxima_exclude_on_edges = False
    is_EDM = False

    # ######### process #########
    # open image

    imp = jimp_dict["IJ"].openImage(image_filename)
    assert imp is not None

    # Overwrite calibration because re-slice will try to interpolate
    imp.getCalibration().pixelDepth = 1
    imp.getCalibration().pixelWidth = 1
    imp.getCalibration().pixelHeight = 1

    # read dataframe with 2D coordinates
    df = pd.read_csv(csv_filename, sep=';')
    df['X'] = df['X'].astype(np.float32)
    df['Y'] = df['Y'].astype(np.float32)
    df['time_point'] = df['time_point'].astype(int)
    df['number'] = df['number'].astype(np.float32)
    points = df[['time_point', 'number', 'X', 'Y']].to_numpy()
    N = points.shape[0]

    print('Number of points: %d' % N)

    # new column
    new_points = -1.0 * np.ones(N)

    # IJ plugin classes
    duplicator = jimp_dict["Duplicator"]()
    slicer = jimp_dict["Slicer"]()

    # counter and lists
    num_failed = 0
    failed_points = []
    rois = []
    results_table = jimp_dict["ResultsTable"].getResultsTable()

    # Loop over points and find their Z-values
    for k in range(N):
        point = points[k, :]  # time-point, number, X, Z

        # create ROI
        roi = jimp_dict["Roi"](
            point[2] - half_region_size[0],
            point[3] - half_region_size[1],
            half_region_size[0] * 2.0,
            half_region_size[1] * 2.0
        )

        # sets the current hyperstack position in image and ROI
        imp.setPosition(target_channel, 1, int(point[0]))  # channel, slice, frame
        roi.setPosition(target_channel, 1, int(point[0]))  # channel, slice, frame
        imp.setRoi(roi)

        dup = duplicator.run(
            imp, target_channel, target_channel, 1, imp.getNSlices(), int(point[0]), int(point[0])
        )
        resliced = slicer.reslice(dup)

        # blur img
        if sigma:
            jimp_dict["GaussianBlur3D"].blur(resliced, sigma, sigma, sigma)

        # z project
        proj = jimp_dict["ZProjector"].run(resliced, 'avg')
        ip = proj.getProcessor()

        # find maximum
        maximum_finder = jimp_dict["MaximumFinder"]()
        maximum_finder.setup('', proj)
        _ = maximum_finder.findMaxima(
            ip, tolerance, False,
            jimp_dict["ImageProcessor"].NO_THRESHOLD, maxima_mode,
            maxima_exclude_on_edges, is_EDM
        )

        # keep looking at https://imagej.nih.gov/ij/developer/source/ij/plugin/filter/MaximumFinder.java.html
        # in analyzeAndMarkMaxima for how PointRoi are setup
        if results_table.size() > 0:
            x = results_table.getValue(0, 0)
            y = results_table.getValue(1, 0)
            pt_roi = jimp_dict["PointRoi"](x, y)

            if debug:
                print([x, y])
                imp.show()
                dup.show()
                resliced.show()
                proj.show()
                proj.setRoi(pt_roi)
                n = True
                while n:
                    x = input("Continue?")
                    n = False if x == "y" or x == "Y" else True

            results_table.reset()

            if pt_roi is not None:
                z_coord = y + 1
                new_points[k] = z_coord  # the z coordinate

                # Set the hyperstack position
                pt_roi.setLocation(point[2], point[3])
                pt_roi.setImage(imp)
                pt_roi.setPosition(target_channel, int(z_coord), int(point[0]))

                # roi_manager.addRoi(pt_roi)
                rois += [pt_roi]
            else:
                print('Invalid PointRoi')
                num_failed += 1
                new_points[k] = np.NAN
                failed_points += [k]
        else:
            print('No results in table for point:')

            if debug:
                print([point[2], point[3]])
                imp.show()
                dup.show()
                resliced.show()
                proj.show()

                n = True
                while n:
                    x = input("Continue?")
                    n = False if x == "y" or x == "Y" else True

            print('No results in table for point:')
            num_failed += 1
            new_points[k] = np.NAN
            failed_points += [k]
            print([k] + new_points[k])

    print('Number of failed detections: %d' % num_failed)

    # add to dataframe and save
    Y_col_idx = df.columns.get_loc("Y")
    df.insert(Y_col_idx + 1, 'Z', new_points.tolist())

    df.to_csv(output_filename, sep=";", index=False)

    print("Done")

    # necessary for ImageJ to actually finish - unbelievable, i know...!
    os.kill(os.getpid(), signal.SIGINT)


def main():
    parser = argparse.ArgumentParser("Finding Z-point for 4D volumes from 2D macrophage segmentation.")
    parser.add_argument("--csv_filename", required=True, type=str,
                        help="Input CSV file where column X,Y, time_point, and number are given.")
    parser.add_argument("--image_filename", required=True, type=str, help="4D input volume as TIF file")
    parser.add_argument("--output_filename", required=True, type=str,
                        help="Filename of the output CSV file. Additional z column will be added to input CSV.")
    parser.add_argument("--sigma", required=False, type=int, default=None,
                        help="Gaussion blur sigma parameter. Usually >2.")
    parser.add_argument("--debug", required=False, default=False,
                        help="switch on debug option. Shows ImageJ interfaces.")

    args = parser.parse_args()

    find_points(args.image_filename, args.csv_filename, args.output_filename, args.sigma, args.debug)


if __name__ == "__main__":
    main()
