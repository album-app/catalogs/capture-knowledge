# Find macrophage points in 3D from 2D solution
This macrophage-brain-vasculature solution finds the 3D coordinates of macrophage provided their coordinates
of a two dimensional max projection. 

## How to run
Install album (https://album.solutions)[https://album.solutions], download the solution file and install and then run it.

## Parameters
The solution has the following parameters:
- `input_filename`: The input filename of the 4D image. The image should be a 4D image with the following dimensions: 
  - 0: Channels
  - 1: x dimension
  - 2: y dimension
  - 3: z dimension
- `csv_filename`: The csv file containing the 2D coordinates of the macrophages
- `output_filename`: The output filename of the csv file containing the 3D coordinates of the macrophages
- `sigma`: The sigma of the gaussian filter used to smooth the image
- `debug`: If true, the intermediate results are shown

## How it works
ImageJ is used to find the 3D coordinates of the macrophages. The 4D image is first smoothed using a gaussian filter. 
Then, the max projection is computed along the z dimension. The 2D coordinates of the macrophages are then used to find
the 3D coordinates of the macrophages. The 3D coordinates are then added to the input csv file. 




