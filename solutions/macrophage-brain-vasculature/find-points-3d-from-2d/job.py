import os
from pathlib import Path
from copy import deepcopy
from joblib import Parallel, delayed
import subprocess

debug = False
override = False
cur_folder = Path(os.path.dirname(os.path.realpath(__file__)))


root = Path("/media/jpa/Volume/MDC/MDC_data/zebrafish_xenograft_model_data/zebrafish_xenograft_model")
#root = Path("/fast/project/PG_VascularImageAnalysis/zebrafish_xenograft_model/")

tif_data_path = root.joinpath("02_Primary_Data")
x_z_csv_paths = [
     root.joinpath("07_Model_Output", "cellpose_segmentation_1dpi_opt_csv"),
     root.joinpath("07_Model_Output", "cellpose_segmentation_5dpi_opt_csv")
]
out_path = root.joinpath("04_Processed_Data", "06_Annotated_Macrophages_3D_opt")

sigma_list = [None, 2, 4, 6]
n_jobs = 1

exclude_tif_list = [
    '200721_LBT123_1dpi_Pos001.tif',
    '200721_LBT123_1dpi_Pos002.tif',
    '200721_LBT123_1dpi_Pos003.tif',
    '200721_LBT123_1dpi_Pos004.tif',
    '200721_LBT123_1dpi_Pos005.tif',
]

exclude_csv_list = [
]


def listdir_fullpath(d):
    return [os.path.join(d, f) for f in os.listdir(d)]


def _check(proc):
    try:
        outs, errs = proc.communicate(timeout=0.5)
        if outs:
            print(outs)
        return True
    except subprocess.TimeoutExpired:
        print("Process %s still running..." % proc.pid)
        return False


def _wait_all(proc_list):
    c = True
    while c:
        r = []
        if proc_list:
            for proc in proc_list:
                r.append(_check(proc))
            c = not all(r)
        else:
            c = False


# get all input tif
tif_list = []
for d in listdir_fullpath(tif_data_path):
    if d.endswith(".tif") and Path(d).name not in exclude_tif_list:
        tif_list.append(d)
tif_list.sort()

print("TIFs found: %s " % len(tif_list))

# get all csv input files
csv_list = []
for c in x_z_csv_paths:
    for d in listdir_fullpath(c):
        if d.endswith(".csv") and not Path(d).name.startswith("analysis_summary") and Path(
                d).name not in exclude_csv_list:
            csv_list.append(d)
csv_list = sorted(csv_list, key=lambda x: Path(x).name)

print("CSVs found: %s " % len(csv_list))

# assert same contents
s1 = [Path(x).stem for x in tif_list]
s2 = [Path(x).stem for x in csv_list]

assert s1 == s2, "For some tif files there is no csv file or other way round: %s" % str(list(set(s1) - set(s2)))

# do a z coordinate determination for each sigma
total_out = []
total_tif = []
total_csv = []
s_list = []
for s in sigma_list:
    s_tif_list = []
    s_csv_list = []

    # define outpaths
    s_out = Path(out_path).joinpath("sigma_%s" % str(s))
    s_out.mkdir(parents=True, exist_ok=True)

    csv_out = [s_out.joinpath(x + ".csv") for x in s1]

    csv_out_list = []
    if not override:
        for idx, p in enumerate(csv_out):
            if not p.exists():
                s_tif_list.append(tif_list[idx])
                s_csv_list.append(csv_list[idx])
                csv_out_list.append(csv_out[idx])

    # merge
    total_out += csv_out_list
    total_tif += s_tif_list
    total_csv += s_csv_list
    s_list += [s] * len(csv_out_list)

if not debug:
    # NOTE: This awful parallelization is necessary due to incompatibility with ImageJ and joblib
    # (or other advanced parallelization libraries). Gaussian Blur causes a memory leak preventing efficient
    # parallelization
    proc_list = []
    run_idx = -1
    for idx, (t, i, o, s) in enumerate(zip(total_tif, total_csv, total_out, s_list)):
        #run_idx += 1
        #if run_idx < n_jobs:
        s = ["--sigma", str(s)] if s is not None else []
        proc = subprocess.Popen(
            [
                "python",
                "find_points_3d_from_2d.py",
                "--image_filename", t,
                "--csv_filename", i,
                "--output_filename", o,
            ] + s,
            shell=False, stdin=None, stdout=None, stderr=None, close_fds=True
        )
        proc_list.append(proc)
        _wait_all(proc_list)
        #else:
        #    _wait_all(proc_list)
        #    run_idx = 0
    # wait for all processes to finish
    #_wait_all(proc_list)

    # this is what i would like to call...
    # Parallel(n_jobs=n_jobs, verbose=10)(
    #    delayed(find_points)(t, i, o, s, False) for t, i, o, s in zip(total_tif, total_csv, total_out, s_list)
    # )
else:
    for t, i, o, s in zip(total_tif, total_csv, total_out, s_list):
        print("find_points(%s, %s, %s, %s, False)" % (t, i, o, s))
