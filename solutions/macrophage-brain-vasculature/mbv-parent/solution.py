from io import StringIO

from album.runner.api import setup

env_file = StringIO("""name:  macrophage-brain-vasculature
channels:
  - conda-forge
  - defaults
dependencies:
  - pyyaml
  - python==3.8
  - pip
  - numpy
  - Pillow
  - pyimagej
  - opencv
  - matplotlib
  - scipy
  - pandas
  - seaborn
  - tifffile
  - scikit-learn
  - openjdk >=11.0.6
  - git
  - pip:
      - requests
      - pyinstaller
""")


setup(
    group="macrophage-brain-vasculature",
    name="mbv-parent",
    version="0.1.0-SNAPSHOT",
    title="macrophage-brain-vasculature parent solution",
    description="A set of solutions for localizing macrophages, segmenting brain vasculature, and tumors",
    authors=["Kyle Harrington", "Jan Philipp Albrecht"],
    cite=[{
        "text": "tbd",
        "doi": "tbd"
    }],
    tags=["vasculature", "segmentation", "projection"],
    license="Apache v2",
    documentation=["README.md"],
    album_api_version="0.4.2",
    args=[],
    dependencies={
        'environment_file': env_file
    }
)
